Preparativi
----
Installare rtl_433. 
* https://github.com/merbanan/rtl_433

Forse serve anche rtl-sdr e gnuradio.
* http://osmocom.org/projects/sdr/wiki/rtl-sdr
* sudo apt-get install gnuradio
 
inserire (come root) in /etc/rc.local la seguente riga
* su pi -c "/home/pi/rtl_433/build/src/rtl_433 -G -F csv | /home/pi/rtl2me.pl > /dev/null &"

Eventuali problemi
----
Installando rtl-sdr ci potrebbero essere dei problemi con i moduli linux.

Verificare se il comando
* sudo rmmod dvb_usb_rtl28xxu

ha successo. In tal caso si può creare (come root) il file 
* rtlsdr.conf

in 
* /etc/modprobe.d

avente come contenuto un'unica riga
* blacklist dvb_usb_rtl28xxu
