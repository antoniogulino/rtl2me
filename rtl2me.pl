#!/usr/bin/perl
use Net::MQTT::Simple "openhab";
use JSON;
use Sys::Hostname;

use strict;

my %model;
$model{'Nexus'}             = 'Nexus Temperature/Humidity';
$model{'Mebus'}             = 'Mebus/433';
$model{'OregonTHGR122N'}    = 'THGR122N'; # lato sud
$model{'OregonRTGN318'}     = 'RTGN318';  # lato nord
$model{'OregonTHN132N'}     = 'THN132N';  # zona garage
$model{'Schraeder'}         = 'Schraeder';
$model{'Brennenstuhl'}      = 'Brennenstuhl RCS 2044';
$model{'PIR'}               = 'Akhan 100F14 remote keyless entry';
$model{'Chuango'}           = 'Chuango Security Technology';
$model{'PIRGenericRemote'}  = 'Generic Remote'; # sono i sensori PIR
$model{'SmokeGS558'}        = 'Smoke detector GS 558'; # probabilmente un PIR



my $OutputDir = $ENV{HOME}."/rtl2me"; #"/home/tomi/rtl2me";
my $TopicRoot = "rtl2me";

my ($yyyy,$mm,$dd);
my ($hour,$min,$sec);
my $ResiduoPrec;
my ($yyyyResiduoPrec, $mmResiduoPrec, $ddResiduoPrec);
my ($hourResiduoPrec, $minResiduoPrec, $secResiduoPrec);

my ($yyyySmokeGS558Prec,$mmSmokeGS558Prec,$ddSmokeGS558Prec);
my ($hourSmokeGS558Prec,$minSmokeGS558Prec,$secSmokeGS558Prec);
my $nrProgrSmokeGS558;
my ($codeSmokeGS558Prec,$keySmokeGS558Prec,$stateSmokeGS558Prec);

my ($yyyyBrennenstuhlPrec,$mmBrennenstuhlPrec,$ddBrennenstuhlPrec);
my ($hourBrennenstuhlPrec,$minBrennenstuhlPrec,$secBrennenstuhlPrec);
my $nrProgrBrennenstuhl;
my ($codeBrennenstuhlPrec,$keyBrennenstuhlPrec,$stateBrennenstuhlPrec);

my ($yyyyChuangoPrec,$mmChuangoPrec,$ddChuangoPrec);
my ($hourChuangoPrec,$minChuangoPrec,$secChuangoPrec);
my $nrProgrChuango;
my ($ChuangoIDPrec, $ChuangoTypPrec, $ChuangoStatusPrec);                    

my ($yyyyPIRGenericRemotePrec,$mmPIRGenericRemotePrec,$ddPIRGenericRemotePrec);
my ($hourPIRGenericRemotePrec,$minPIRGenericRemotePrec,$secPIRGenericRemotePrec);
my $nrProgrPIRGenericRemote;
my ($PIRGenericRemoteIDPrec, $PIRGenericRemoteTypPrec, $PIRGenericRemoteStatusPrec);                    

my ($yyyySchraederPrec,$mmSchraederPrec,$ddSchraederPrec);
my ($hourSchraederPrec,$minSchraederPrec,$secSchraederPrec);
my $nrProgrSchraeder;
my ($SchraederIDPrec, $SchraederTypPrec, $SchraederStatusPrec);                    

my ($yyyyNexusPrec,$mmNexusPrec,$ddNexusPrec);
my ($hourNexusPrec,$minNexusPrec,$secNexusPrec);
my $nrProgrNexus;
my ($NexusIDPrec, $NexusChannelPrec, $NexusStatusPrec, $NexusTempPrec, $NexusHumidityPrec);                    

my ($yyyyMebusPrec,$mmMebusPrec,$ddMebusPrec);
my ($hourMebusPrec,$minMebusPrec,$secMebusPrec);
my $nrProgrMebus;
my ($MebusIDPrec, $MebusChannelPrec, $MebusStatusPrec, $MebusTempPrec, $MebusHumidityPrec);                    

my ($yyyyPirPrec,$mmPirPrec,$ddPirPrec);
my ($hourPirPrec,$minPirPrec,$secPirPrec);
my $nrProgrPir;
my $PirIDPrec;                    

my ($yyyyOregonRTGN318Prec,$mmOregonRTGN318Prec,$ddOregonRTGN318Prec);
my ($hourOregonRTGN318Prec,$minOregonRTGN318Prec,$secOregonRTGN318Prec);
my $nrProgrOregonRTGN318;
my ($OregonRTGN318IDPrec, $OregonRTGN318ChannelPrec, $OregonRTGN318StatusPrec, $OregonRTGN318TempPrec, $OregonRTGN318HumidityPrec);                    

my ($yyyyOregonTHGR122NPrec,$mmOregonTHGR122NPrec,$ddOregonTHGR122NPrec);
my ($hourOregonTHGR122NPrec,$minOregonTHGR122NPrec,$secOregonTHGR122NPrec);
my $nrProgrOregonTHGR122N;
my ($OregonTHGR122NIDPrec, $OregonTHGR122NChannelPrec, $OregonTHGR122NStatusPrec, $OregonTHGR122NTempPrec, $OregonTHGR122NHumidityPrec);                    

my ($yyyyOregonTHN132NPrec,$mmOregonTHN132NPrec,$ddOregonTHN132NPrec);
my ($hourOregonTHN132NPrec,$minOregonTHN132NPrec,$secOregonTHN132NPrec);
my $nrProgrOregonTHN132N;
my ($OregonTHN132NIDPrec, $OregonTHN132NChannelPrec, $OregonTHN132NStatusPrec, $OregonTHN132NTempPrec);                    


sub mqtt_pub {
    my $brokerhost  = "openhab";
    my $mytopic = shift;
    my $mymsg   = shift;
    $mymsg =~ s/"/\\"/g;
    system("mosquitto_pub --retain -h $brokerhost -t $mytopic -m \"$mymsg\"  --qos 2 &");
}



while(<STDIN>){
    my %json = ("gateway" => hostname);
       $json{"script"} = $0;
    my $topic = $TopicRoot;
    if (s/^20(1[7-9]|2\d)\-(0[1-9]|1[0-2])\-(0[1-9]|[12]\d|3[01]) ([0-2]\d):([0-5]\d):([0-5]\d).//) {
        $yyyy = '20'.$1 + 0;
        $mm = $2 +0;
        $dd = $3 +0;
        $hour = $4 +0;
        $min  = $5 +0;
        $sec  = $6 +0;
	    $json{"dd"} = $dd;
	    $json{"mm"} = $mm;
	    $json{"yyyy"} = $yyyy;
	    $json{"hour"} = $hour;
	    $json{"min"} = $min;
	    $json{"sec"} = $sec;
   #     my $timestamp = sprintf "%02d:%02d:%02d",$hour,$min,$sec;
   #     my $datestamp = "$yyyy$mm$dd";
        if (s/^$model{'SmokeGS558'},//) {
	    $json{"modelID"} = 'SmokeGS558';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^(\d\d*),+,2,+,0,+,([0-9af]{6}),+//) {
                my $code = $1;
                my $key  = $2;
          
	        $topic .= "/$key";
		$json{"code"} = $code;
		$json{"key"} = $key;
                if ($yyyy ne $yyyySmokeGS558Prec or $mm ne $mmSmokeGS558Prec or $dd ne $ddSmokeGS558Prec
                    or $hour ne $hourSmokeGS558Prec or $min ne $minSmokeGS558Prec or $sec ne $secSmokeGS558Prec
                    or $codeSmokeGS558Prec ne $code or $keySmokeGS558Prec ne $key 
                    ) {
			    
                    if ($yyyy eq $yyyySmokeGS558Prec and $mm eq $mmSmokeGS558Prec and $dd eq $ddSmokeGS558Prec
                        and $hour eq $hourSmokeGS558Prec and $min eq $minSmokeGS558Prec and $sec eq $secSmokeGS558Prec
                        ) {
                        $nrProgrSmokeGS558++;
                    } else {
                        $nrProgrSmokeGS558 = 1;                    
                    }
		    $json{"seq"} = $nrProgrSmokeGS558;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = sprintf "$OutputDir/SmokeGS558-%04d%02d.tsv",$yyyy,$mm;
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tcode\tkey\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
 
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrSmokeGS558\t".(sprintf "%05d",$code)."\t$key\n";
                        close F;
                        $yyyySmokeGS558Prec  = $yyyy;
                        $mmSmokeGS558Prec    = $mm;
                        $ddSmokeGS558Prec    = $dd;
                        $hourSmokeGS558Prec  = $hour;
                        $minSmokeGS558Prec   = $min;
                        $secSmokeGS558Prec   = $sec;
                        $codeSmokeGS558Prec  = $code;
                        $keySmokeGS558Prec   = $key;
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                }
	    }

        } elsif (s/^$model{'Brennenstuhl'}: //) {
	    $json{"modelID"} = 'Brennenstuhl';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^system code: ([01]{5})\. key: ([ABCDE])\, state: (ON|OFF)//) {
                my $code = $1;
                my $key  = $2;
                my $state = $3;
                my $mycode = sprintf "%05d",$code;
	        $topic .= "/$mycode/$key";
		$json{"code"} = $code;
		$json{"key"} = $key;
		$json{"state"} = $state;
                if ($yyyy ne $yyyyBrennenstuhlPrec or $mm ne $mmBrennenstuhlPrec or $dd ne $ddBrennenstuhlPrec
                    or $hour ne $hourBrennenstuhlPrec or $min ne $minBrennenstuhlPrec or $sec ne $secBrennenstuhlPrec
                    or $codeBrennenstuhlPrec ne $code or $keyBrennenstuhlPrec ne $key or $stateBrennenstuhlPrec ne $state
                    ) {
                    if ($yyyy eq $yyyyBrennenstuhlPrec and $mm eq $mmBrennenstuhlPrec and $dd eq $ddBrennenstuhlPrec
                        and $hour eq $hourBrennenstuhlPrec and $min eq $minBrennenstuhlPrec and $sec eq $secBrennenstuhlPrec
                        ) {
                        $nrProgrBrennenstuhl++;
                    } else {
                        $nrProgrBrennenstuhl = 1;                    
                    }
		    $json{"seq"} = $nrProgrBrennenstuhl;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = sprintf "$OutputDir/Brennenstuhl-%04d%02d.tsv",$yyyy,$mm;
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tcode\tkey\tstate\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
 
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrBrennenstuhl\t".(sprintf "%05d",$code)."\t$key\t$state\n";
                        close F;
                        $yyyyBrennenstuhlPrec  = $yyyy;
                        $mmBrennenstuhlPrec    = $mm;
                        $ddBrennenstuhlPrec    = $dd;
                        $hourBrennenstuhlPrec  = $hour;
                        $minBrennenstuhlPrec   = $min;
                        $secBrennenstuhlPrec   = $sec;
                        $codeBrennenstuhlPrec  = $code;
                        $keyBrennenstuhlPrec   = $key;
                        $stateBrennenstuhlPrec = $state;                    
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                }
            }
            
        } elsif (s/^$model{'Chuango'},//) {
	    $json{"modelID"} = 'Chuango';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^(\d+),,+$//) {
                my $ChuangoID     = $1;
	        $topic .= "/$ChuangoID";
		$json{"ID"} = $ChuangoID;
                if ($yyyy ne $yyyyChuangoPrec or $mm ne $mmChuangoPrec or $dd ne $ddChuangoPrec
                    or $hour ne $hourChuangoPrec or $min ne $minChuangoPrec or $sec ne $secChuangoPrec
                    or $ChuangoIDPrec ne $ChuangoID 
                    ) {
                    if ($yyyy eq $yyyyChuangoPrec and $mm eq $mmChuangoPrec and $dd eq $ddChuangoPrec
                        and $hour eq $hourChuangoPrec and $min eq $minChuangoPrec and $sec eq $secChuangoPrec
                        ) {
                        $nrProgrChuango++;
                    } else {
                        $nrProgrChuango = 1;                    
                    }
		    $json{"seq"} = $nrProgrChuango;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = sprintf "$OutputDir/Chuango-%04d%02d.tsv",$yyyy,$mm;
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tID\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrChuango\t$ChuangoID\n";
                        close F;
                        $yyyyChuangoPrec = $yyyy;
                        $mmChuangoPrec   = $mm;
                        $ddChuangoPrec   = $dd;
                        $hourChuangoPrec = $hour;
                        $minChuangoPrec  = $min;
                        $secChuangoPrec  = $sec;
                        $ChuangoIDPrec   = $ChuangoID;
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                }
            }
                
        } elsif (s/^$model{'PIRGenericRemote'},//) {
            $json{"modelID"} = 'PIRGenericRemote';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};

            if (s/^(\d+),,+$//) {
		my $PIRGenericRemoteID = $1;
	        $topic .= "/$PIRGenericRemoteID";
                $json{"ID"} = $PIRGenericRemoteID;
                if ($yyyy ne $yyyyPIRGenericRemotePrec or $mm ne $mmPIRGenericRemotePrec or $dd ne $ddPIRGenericRemotePrec
                    or $hour ne $hourPIRGenericRemotePrec or $min ne $minPIRGenericRemotePrec or $sec ne $secPIRGenericRemotePrec
                    or $PIRGenericRemoteIDPrec ne $PIRGenericRemoteID
                    ) {
                    if ($yyyy eq $yyyyPIRGenericRemotePrec and $mm eq $mmPIRGenericRemotePrec and $dd eq $ddPIRGenericRemotePrec
                        and $hour eq $hourPIRGenericRemotePrec and $min eq $minPIRGenericRemotePrec and $sec eq $secPIRGenericRemotePrec
                        ) {
                        $nrProgrPIRGenericRemote++;
                    } else {
                        $nrProgrPIRGenericRemote = 1;                    
                    }
		    $json{"seq"} = $nrProgrPIRGenericRemote;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = "$OutputDir/PIRGenericRemote-$PIRGenericRemoteID-".(sprintf "%04d%02d.tsv",$yyyy,$mm);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tID\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrPIRGenericRemote\t$PIRGenericRemoteID\n";
                        close F;
                        $yyyyPIRGenericRemotePrec = $yyyy;
                        $mmPIRGenericRemotePrec   = $mm;
                        $ddPIRGenericRemotePrec   = $dd;
                        $hourPIRGenericRemotePrec = $hour;
                        $minPIRGenericRemotePrec  = $min;
                        $secPIRGenericRemotePrec  = $sec;
                        $PIRGenericRemoteIDPrec  = $PIRGenericRemoteID;
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                }
	    }
        } elsif (s/^$model{'Schraeder'},//) {
            $json{"modelID"} = 'Schraeder';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};

            if (s/^([0-9ABCDEF]+),,,,(\w+),+(\w+),+$//) {
                my $SchraederID     = $1;
                my $SchraederStatus = $2;
                my $SchraederTyp    = $3;
	        $topic .= "/$SchraederTyp/$SchraederID";
                $json{"ID"} = $SchraederID;
                $json{"Status"} = $SchraederStatus;
                $json{"Typ"} = $SchraederTyp;
                if ($yyyy ne $yyyySchraederPrec or $mm ne $mmSchraederPrec or $dd ne $ddSchraederPrec
                    or $hour ne $hourSchraederPrec or $min ne $minSchraederPrec or $sec ne $secSchraederPrec
                    or $SchraederIDPrec ne $SchraederID or $SchraederTypPrec ne $SchraederTyp 
                    or $SchraederStatusPrec ne $SchraederStatus
                    ) {
                    if ($yyyy eq $yyyySchraederPrec and $mm eq $mmSchraederPrec and $dd eq $ddSchraederPrec
                        and $hour eq $hourSchraederPrec and $min eq $minSchraederPrec and $sec eq $secSchraederPrec
                        ) {
                        $nrProgrSchraeder++;
                    } else {
                        $nrProgrSchraeder = 1;                    
                    }
		    $json{"seq"} = $nrProgrSchraeder;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = "$OutputDir/Schraeder-".(sprintf "%04d.tsv",$yyyy);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tID\tstatus\tTyp\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrSchraeder\t$SchraederID\t\t$SchraederStatus\t$SchraederTyp\n";
                        close F;
                        $yyyySchraederPrec = $yyyy;
                        $mmSchraederPrec   = $mm;
                        $ddSchraederPrec   = $dd;
                        $hourSchraederPrec = $hour;
                        $minSchraederPrec  = $min;
                        $secSchraederPrec  = $sec;
                        $SchraederIDPrec  = $SchraederID;
                        $SchraederTypPrec   = $SchraederTyp;
                        $SchraederStatusPrec = $SchraederStatus;                    
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                }
            }
                
        } elsif (s/^$model{'Nexus'},//) {
	    $json{"modelID"} = 'Nexus';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^(\d+),(\d+),(\w+),(-?\d+\.?\d*),,,,(\d+),+$//) {
                my $NexusID = $1;
                my $NexusChannel  = $2;
                my $NexusStatus   = $3;
                my $NexusTemp     = $4 +0;
                my $NexusHumidity = $5 +0;
	        $topic .= "/ch$NexusChannel/$NexusID";
                $json{"ID"} = $NexusID;
                $json{"channel"} = $NexusChannel;
                $json{"status"} = $NexusStatus;
                $json{"temp"} = $NexusTemp;
	        $json{"humidity"} = $NexusHumidity;
		mqtt_pub("$topic/status",$json{"status"});
		mqtt_pub("$topic/temp",$json{"temp"});
		mqtt_pub("$topic/humidity",$json{"humidity"});
                if ($yyyy ne $yyyyNexusPrec or $mm ne $mmNexusPrec or $dd ne $ddNexusPrec
                    or $hour ne $hourNexusPrec or $min ne $minNexusPrec or $sec ne $secNexusPrec
                    or $NexusIDPrec ne $NexusID or $NexusChannelPrec ne $NexusChannel 
                    or $NexusStatusPrec ne $NexusStatus
                    or $NexusTempPrec ne $NexusTemp or $NexusHumidityPrec ne $NexusHumidity 
                    ) {
                    if ($yyyy eq $yyyyNexusPrec and $mm eq $mmNexusPrec and $dd eq $ddNexusPrec
                        and $hour eq $hourNexusPrec and $min eq $minNexusPrec and $sec eq $secNexusPrec
                        and $NexusIDPrec eq $NexusID and $NexusChannelPrec eq $NexusChannel 
                        ) {
                        $nrProgrNexus++;
                    } else {
                        $nrProgrNexus = 1;                    
                    }
		    $json{"seq"} = $nrProgrNexus;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = "$OutputDir/Nexus-$NexusID-$NexusChannel-".(sprintf "%04d%02d.tsv",$yyyy,$mm);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tbattery\ttemp\thumidity\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrNexus\t$NexusStatus\t$NexusTemp\t$NexusHumidity\n";
                        close F;
                        $yyyyNexusPrec  = $yyyy;
                        $mmNexusPrec    = $mm;
                        $ddNexusPrec    = $dd;
                        $hourNexusPrec  = $hour;
                        $minNexusPrec   = $min;
                        $secNexusPrec   = $sec;
                        $NexusIDPrec  = $NexusID;
                        $NexusChannelPrec   = $NexusChannel;
                        $NexusStatusPrec = $NexusStatus;                    
                        $NexusTempPrec = $NexusTemp;                    
                        $NexusHumidityPrec = $NexusHumidity;                    
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                    
                }                
            }
            
        } elsif (s/^$model{'Mebus'},//) {
	    $json{"modelID"} = 'Mebus';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^(\d+),(\d+),(\w+),(-?\d+\.?\d*),,,,(\d+),+$//) {
                my $MebusID = $1;
                my $MebusChannel  = $2;
                my $MebusStatus   = $3;
                my $MebusTemp     = $4 +0;
                my $MebusHumidity = $5 +0;
	        $topic .= "/ch$MebusChannel/$MebusID";
                $json{"ID"} = $MebusID;
                $json{"channel"} = $MebusChannel;
                $json{"status"} = $MebusStatus;
                $json{"temp"} = $MebusTemp;
	        $json{"humidity"} = $MebusHumidity;
		mqtt_pub("$topic/status",$json{"status"});
		mqtt_pub("$topic/temp",$json{"temp"});
		mqtt_pub("$topic/humidity",$json{"humidity"});
                if ($yyyy ne $yyyyMebusPrec or $mm ne $mmMebusPrec or $dd ne $ddMebusPrec
                    or $hour ne $hourMebusPrec or $min ne $minMebusPrec or $sec ne $secMebusPrec
                    or $MebusIDPrec ne $MebusID or $MebusChannelPrec ne $MebusChannel 
                    or $MebusStatusPrec ne $MebusStatus
                    or $MebusTempPrec ne $MebusTemp or $MebusHumidityPrec ne $MebusHumidity 
                    ) {
                    if ($yyyy eq $yyyyMebusPrec and $mm eq $mmMebusPrec and $dd eq $ddMebusPrec
                        and $hour eq $hourMebusPrec and $min eq $minMebusPrec and $sec eq $secMebusPrec
                        and $MebusIDPrec eq $MebusID and $MebusChannelPrec eq $MebusChannel 
                        ) {
                        $nrProgrMebus++;
                    } else {
                        $nrProgrMebus = 1;                    
                    }
		    $json{"seq"} = $nrProgrMebus;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = "$OutputDir/Mebus-$MebusID-$MebusChannel-".(sprintf "%04d%02d.tsv",$yyyy,$mm);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tbattery\ttemp\thumidity\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrMebus\t$MebusStatus\t$MebusTemp\t$MebusHumidity\n";
                        close F;
                        $yyyyMebusPrec  = $yyyy;
                        $mmMebusPrec    = $mm;
                        $ddMebusPrec    = $dd;
                        $hourMebusPrec  = $hour;
                        $minMebusPrec   = $min;
                        $secMebusPrec   = $sec;
                        $MebusIDPrec  = $MebusID;
                        $MebusChannelPrec   = $MebusChannel;
                        $MebusStatusPrec = $MebusStatus;                    
                        $MebusTempPrec = $MebusTemp;                    
                        $MebusHumidityPrec = $MebusHumidity;                    
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                    
                }
                
            }
            
        } elsif (s/^$model{'OregonTHGR122N'},//) {
	    $json{"modelID"} = 'OregonTHGR122N';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^(\d+),(\d+),(\w+),(-?\d+\.?\d*),,,,(\d+),,+OS,+$//) {
                my $OregonTHGR122NID = $1;
                my $OregonTHGR122NChannel  = $2;
                my $OregonTHGR122NStatus   = $3;
                my $OregonTHGR122NTemp     = $4 +0;
                my $OregonTHGR122NHumidity = $5 +0;
	        $topic .= "/ch$OregonTHGR122NChannel/$OregonTHGR122NID";
                $json{"ID"} = $OregonTHGR122NID;
                $json{"channel"} = $OregonTHGR122NChannel;
                $json{"status"} = $OregonTHGR122NStatus;
                $json{"temp"} = $OregonTHGR122NTemp;
	        $json{"humidity"} = $OregonTHGR122NHumidity;
		mqtt_pub("$topic/status",$json{"status"});
		mqtt_pub("$topic/temp",$json{"temp"});
		mqtt_pub("$topic/humidity",$json{"humidity"});

                if ($yyyy ne $yyyyOregonTHGR122NPrec or $mm ne $mmOregonTHGR122NPrec or $dd ne $ddOregonTHGR122NPrec
                    or $hour ne $hourOregonTHGR122NPrec or $min ne $minOregonTHGR122NPrec or $sec ne $secOregonTHGR122NPrec
                    or $OregonTHGR122NIDPrec ne $OregonTHGR122NID or $OregonTHGR122NChannelPrec ne $OregonTHGR122NChannel 
                    or $OregonTHGR122NStatusPrec ne $OregonTHGR122NStatus
                    or $OregonTHGR122NTempPrec ne $OregonTHGR122NTemp or $OregonTHGR122NHumidityPrec ne $OregonTHGR122NHumidity 
                    ) {
                    if ($yyyy eq $yyyyOregonTHGR122NPrec and $mm eq $mmOregonTHGR122NPrec and $dd eq $ddOregonTHGR122NPrec
                        and $hour eq $hourOregonTHGR122NPrec and $min eq $minOregonTHGR122NPrec and $sec eq $secOregonTHGR122NPrec
                        and $OregonTHGR122NIDPrec eq $OregonTHGR122NID and $OregonTHGR122NChannelPrec eq $OregonTHGR122NChannel 
                        ) {
                        $nrProgrOregonTHGR122N++;
                    } else {
                        $nrProgrOregonTHGR122N = 1;                    
                    }
		    $json{"seq"} = $nrProgrOregonTHGR122N;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = "$OutputDir/OregonTHGR122N-$OregonTHGR122NID-$OregonTHGR122NChannel-".(sprintf "%04d%02d.tsv",$yyyy,$mm);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tbattery\ttemp\thumidity\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrOregonTHGR122N\t$OregonTHGR122NStatus\t$OregonTHGR122NTemp\t$OregonTHGR122NHumidity\n";
                        close F;
                        $yyyyOregonTHGR122NPrec  = $yyyy;
                        $mmOregonTHGR122NPrec    = $mm;
                        $ddOregonTHGR122NPrec    = $dd;
                        $hourOregonTHGR122NPrec  = $hour;
                        $minOregonTHGR122NPrec   = $min;
                        $secOregonTHGR122NPrec   = $sec;
                        $OregonTHGR122NIDPrec  = $OregonTHGR122NID;
                        $OregonTHGR122NChannelPrec   = $OregonTHGR122NChannel;
                        $OregonTHGR122NStatusPrec = $OregonTHGR122NStatus;                    
                        $OregonTHGR122NTempPrec = $OregonTHGR122NTemp;                    
                        $OregonTHGR122NHumidityPrec = $OregonTHGR122NHumidity;                    
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                    
                }
                
            }
            
        } elsif (s/^$model{'OregonRTGN318'},//) {
	    $json{"modelID"} = 'OregonRTGN318';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^(\d+),(\d+),(\w+),(-?\d+\.?\d*),,,,(\d+),+,(\d+\.?\d*),+OS,+$//) {
                my $OregonRTGN318ID = $1;
                my $OregonRTGN318Channel  = $2;
                my $OregonRTGN318Status   = $3;
                my $OregonRTGN318Temp     = $4 +0;
                my $OregonRTGN318Humidity = $5;
	        $topic .= "/ch$OregonRTGN318Channel/$OregonRTGN318ID";
                $json{"ID"} = $OregonRTGN318ID;
                $json{"channel"} = $OregonRTGN318Channel;
                $json{"status"} = $OregonRTGN318Status;
                $json{"temp"} = $OregonRTGN318Temp;
	        $json{"humidity"} = $OregonRTGN318Humidity;
		mqtt_pub("$topic/status",$json{"status"});
		mqtt_pub("$topic/temp",$json{"temp"});
		mqtt_pub("$topic/humidity",$json{"humidity"});

                if ($yyyy ne $yyyyOregonRTGN318Prec or $mm ne $mmOregonRTGN318Prec or $dd ne $ddOregonRTGN318Prec
                    or $hour ne $hourOregonRTGN318Prec or $min ne $minOregonRTGN318Prec or $sec ne $secOregonRTGN318Prec
                    or $OregonRTGN318IDPrec ne $OregonRTGN318ID or $OregonRTGN318ChannelPrec ne $OregonRTGN318Channel 
                    or $OregonRTGN318StatusPrec ne $OregonRTGN318Status
                    or $OregonRTGN318TempPrec ne $OregonRTGN318Temp or $OregonRTGN318HumidityPrec ne $OregonRTGN318Humidity 
                    ) {
                    if ($yyyy eq $yyyyOregonRTGN318Prec and $mm eq $mmOregonRTGN318Prec and $dd eq $ddOregonRTGN318Prec
                        and $hour eq $hourOregonRTGN318Prec and $min eq $minOregonRTGN318Prec and $sec eq $secOregonRTGN318Prec
                        and $OregonRTGN318IDPrec eq $OregonRTGN318ID and $OregonRTGN318ChannelPrec eq $OregonRTGN318Channel 
                        ) {
                        $nrProgrOregonRTGN318++;
                    } else {
                        $nrProgrOregonRTGN318 = 1;                    
                    }
		    $json{"seq"} = $nrProgrOregonRTGN318;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = "$OutputDir/OregonRTGN318-$OregonRTGN318ID-$OregonRTGN318Channel-".(sprintf "%04d%02d.tsv",$yyyy,$mm);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tbattery\ttemp\thumidity\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrOregonRTGN318\t$OregonRTGN318Status\t$OregonRTGN318Temp\t$OregonRTGN318Humidity\n";
                        close F;
                        $yyyyOregonRTGN318Prec  = $yyyy;
                        $mmOregonRTGN318Prec    = $mm;
                        $ddOregonRTGN318Prec    = $dd;
                        $hourOregonRTGN318Prec  = $hour;
                        $minOregonRTGN318Prec   = $min;
                        $secOregonRTGN318Prec   = $sec;
                        $OregonRTGN318IDPrec  = $OregonRTGN318ID;
                        $OregonRTGN318ChannelPrec   = $OregonRTGN318Channel;
                        $OregonRTGN318StatusPrec = $OregonRTGN318Status;                    
                        $OregonRTGN318TempPrec = $OregonRTGN318Temp;                    
                        $OregonRTGN318HumidityPrec = $OregonRTGN318Humidity;                    
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                    
                }
                
            }
            
        } elsif (s/^$model{'OregonTHN132N'},//) {
	    $json{"modelID"} = 'OregonTHN132N';
	    $json{"model"} = $model{$json{"modelID"}};
            $topic .= "/".$json{"modelID"};
            if (s/^(\d+),(\d+),(\w+),(-?\d+\.?\d*),,,,,,,,,,,,,,,,,\d+\.\d+,+OS,$//
	     or s/^(\d+),(\d+),(\w+),(-?\d+\.?\d*),,,,,,OS,+$//) {
                my $OregonTHN132NID = $1;
                my $OregonTHN132NChannel  = $2;
                my $OregonTHN132NStatus   = $3;
                my $OregonTHN132NTemp     = $4 +0;
	        $topic .= "/ch$OregonTHN132NChannel/$OregonTHN132NID";
                $json{"ID"} = $OregonTHN132NID;
                $json{"channel"} = $OregonTHN132NChannel;
                $json{"status"} = $OregonTHN132NStatus;
                $json{"temp"} = $OregonTHN132NTemp;
		mqtt_pub("$topic/status",$json{"status"});
		mqtt_pub("$topic/temp",$json{"temp"});
                if ($yyyy ne $yyyyOregonTHN132NPrec or $mm ne $mmOregonTHN132NPrec or $dd ne $ddOregonTHN132NPrec
                    or $hour ne $hourOregonTHN132NPrec or $min ne $minOregonTHN132NPrec or $sec ne $secOregonTHN132NPrec
                    or $OregonTHN132NIDPrec ne $OregonTHN132NID or $OregonTHN132NChannelPrec ne $OregonTHN132NChannel 
                    or $OregonTHN132NStatusPrec ne $OregonTHN132NStatus
                    or $OregonTHN132NTempPrec ne $OregonTHN132NTemp 
                    ) {
                    if ($yyyy eq $yyyyOregonTHN132NPrec and $mm eq $mmOregonTHN132NPrec and $dd eq $ddOregonTHN132NPrec
                        and $hour eq $hourOregonTHN132NPrec and $min eq $minOregonTHN132NPrec and $sec eq $secOregonTHN132NPrec
                        and $OregonTHN132NIDPrec eq $OregonTHN132NID and $OregonTHN132NChannelPrec eq $OregonTHN132NChannel 
                        ) {
                        $nrProgrOregonTHN132N++;
                    } else {
                        $nrProgrOregonTHN132N = 1;                    
                    }
		    $json{"seq"} = $nrProgrOregonTHN132N;
		    mqtt_pub($topic,encode_json(\%json));

                    my $filename = "$OutputDir/OregonTHN132N-$OregonTHN132NID-$OregonTHN132NChannel-".(sprintf "%04d%02d.tsv",$yyyy,$mm);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tbattery\ttemp\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrOregonTHN132N\t$OregonTHN132NStatus\t$OregonTHN132NTemp\n";
                        close F;
                        $yyyyOregonTHN132NPrec  = $yyyy;
                        $mmOregonTHN132NPrec    = $mm;
                        $ddOregonTHN132NPrec    = $dd;
                        $hourOregonTHN132NPrec  = $hour;
                        $minOregonTHN132NPrec   = $min;
                        $secOregonTHN132NPrec   = $sec;
                        $OregonTHN132NIDPrec  = $OregonTHN132NID;
                        $OregonTHN132NChannelPrec   = $OregonTHN132NChannel;
                        $OregonTHN132NStatusPrec = $OregonTHN132NStatus;                    
                        $OregonTHN132NTempPrec = $OregonTHN132NTemp;                    
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                    
                }
                
            }
            
        #} elsif (s/,*$model{'PIR'},*$//) {
        } elsif (0) {
            if (s/^,*(\d+)$//) {
                my $PirID = $1;
                if ($yyyy ne $yyyyPirPrec or $mm ne $mmPirPrec or $dd ne $ddPirPrec
                    or $hour ne $hourPirPrec or $min ne $minPirPrec or $sec ne $secPirPrec
                    or $PirIDPrec ne $PirID
                    ) {
                    if ($yyyy eq $yyyyPirPrec and $mm eq $mmPirPrec and $dd eq $ddPirPrec
                        and $hour eq $hourPirPrec and $min eq $minPirPrec and $sec eq $secPirPrec
                        and $PirIDPrec eq $PirID 
                        ) {
                        $nrProgrPir++;
                    } else {
                        $nrProgrPir = 1;                    
                    }
                    my $filename = "$OutputDir/Pir-$PirID-".(sprintf "%04d%02d.tsv",$yyyy,$mm);
                    unless (-e $filename) {
                        open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                        print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\n";
                        close F;
                    }
                    if (open F, ">>$filename") {
                        print F "$dd\t$mm\t$yyyy\t$hour\t$min\t$sec\t$nrProgrPir\n";
                        close F;
                        $yyyyPirPrec  = $yyyy;
                        $mmPirPrec    = $mm;
                        $ddPirPrec    = $dd;
                        $hourPirPrec  = $hour;
                        $minPirPrec   = $min;
                        $secPirPrec   = $sec;
                        $PirIDPrec  = $PirID;
                    } else {
                        warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                    }
                    
                }
                
            }
        } else {
            unless ($yyyy and $mm and $dd) {
                my @lt = localtime();
                $yyyy = 1900 + $lt[5];
                $mm   =    1 + $lt[4];
                $dd   =        $lt[3];
                $hour =        $lt[2];
                $min  =        $lt[1];
                $sec  =        $lt[0];
            }
            unless (/^RF-tech,0,,LOW,+0,+[0\.]+,+$/
                    or /^Generic temperature sensor 1,0,,0,0\.000,+$/) {
                my $filename = "$OutputDir/rtl-residuo-".(sprintf "%04d%02d.csv",$yyyy,$mm);
                unless (-e $filename) {
                    open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                    print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tbattery\ttemp\thumidity\n";
                    close F;
                }
                $_ = ",$_";
                s/\,\,(AcuRite Rain|Raw Message|,Waveman Switch|Energy Count|time,)/\t,$1/;
                my $riga = sprintf "%04d-%02d-%02d %02d:%02d:%02d%1s",$yyyy,$mm,$dd,$hour,$min,$sec,$_;
                if (open F, ">>$filename") {
                    print F $riga;#"%04d-%02d-%02d %02d:%02d:%02d,",$yyyy,$mm,$dd,$hour,$min,$sec;
                    #print F;
                    close F;
                } else {
                    warn "timestamp: Non riesco ad aprire in append il file $filename\n";
                }
            }
        }

    } else {
        my @lt = localtime();
        $yyyy = 1900 + $lt[5];
        $mm   =    1 + $lt[4];
        $dd   =        $lt[3];
        $hour =        $lt[2];
        $min  =        $lt[1];
        $sec  =        $lt[0];
        if ($yyyy ne $yyyyResiduoPrec or $mm ne $mmResiduoPrec or $dd ne $ddResiduoPrec
            or $hour ne $hourResiduoPrec or $min ne $minResiduoPrec or $sec ne $secResiduoPrec
            or $_ ne $ResiduoPrec
            ){
            my $filename = "$OutputDir/rtl-residuo-".(sprintf "%04d%02d.csv",$yyyy,$mm);
            unless (-e $filename) {
                open F,">$filename" or warn "timestamp: Non riesco a creare il file $filename\n";
                print F "giorno\tmese\tanno\thh\tmin\tsec\tseq\tbattery\ttemp\thumidity\n";
                close F;
            }
            if (open F, ">>$filename") {
                printf F "%04d-%02d-%02d %02d:%02d:%02d\t,",$yyyy,$mm,$dd,$hour,$min,$sec;
                print F;
                close F;
                $yyyyResiduoPrec  = $yyyy;
                $mmResiduoPrec    = $mm;
                $ddResiduoPrec    = $dd;
                $hourResiduoPrec  = $hour;
                $minResiduoPrec   = $min;
                $secResiduoPrec   = $sec;                
                $ResiduoPrec      = $_;
            } else {
                warn "timestamp: Non riesco ad aprire in append il file $filename\n";
            }
        }
    }   
}
1;
